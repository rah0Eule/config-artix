#!/bin/sh
set -euo pipefail
doas cp -r etc/ /
rsync -av dotfiles/. ~
echo -e "tmpfs   /home/$USER/.cache      tmpfs   noatime,nodev,nosuid,size=2G  0       0
tmpfs	/tmp	tmpfs	rw,nodev,nosuid	0	0
tmpfs	/var/log	tmpfs	rw,nodev,nosuid,size=1G	0	0" | doas tee -a /etc/fstab
doas chmod 700 /boot /etc/{iptables,nftables.conf} || true
doas pacman -S virt-manager --noconfirm
doas gpasswd -a $USER libvirt
doas chmod -c 0400 /etc/doas.conf
