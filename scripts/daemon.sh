#!/bin/bash
set -euo pipefail
# doas rsv list --enabled | grep -v agetty | awk -F: '{print $2}' |sed 's|\ ||g' |sort -f > daemon.sh
doas rsv enable alsa
doas rsv enable ananicy-cpp
doas rsv enable avahi-daemon
doas rsv enable backlight
doas rsv enable bluetoothd
doas rsv enable chrony
#doas rsv enable cpupower
doas rsv enable cupsd
doas rsv enable dbus
doas rsv enable dnscrypt-proxy
doas rsv enable firewalld
doas rsv enable libvirtd
doas rsv enable logind
doas rsv enable NetworkManager
doas rsv enable profile-sync-daemon
doas rsv enable tailscaled
doas rsv enable thermald
doas rsv enable tlp
doas rsv enable udevd
doas rsv enable usbguard
doas rsv enable virtlockd
doas rsv enable virtlogd
doas rsv enable zramen
doas rsv enable cronie
doas rsv enable syslog-ng

