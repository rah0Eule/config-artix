# DPI and scale

- 96dpi = 1.0

- 144dpi = 1.5

- 物理DPI: 1920px / 293mm * 25.4mm/inch = 166dot/inch

# X11

## i3

i3全体をスケール、i3bar, i3lockもスケール

https://gitlab.com/takuoh/config-artix/-/blob/3530e253e1aed90b8bcf0eb286ed35bfed15b269/dotfiles/.xinitrc#L3

## KDE

KScreenを使う

不要な変数はenvの-uオプションでunsetさせる

https://gitlab.com/takuoh/config-artix/-/blob/301eb4d18668edec81c0651a9c9e08234c1cb776/dotfiles/.xinitrc#L47

## GTK3

thunar, nemoなどのGTK3アプリケーションはフォントのみスケール

firefox, chromium, electronはUIもスケール

https://gitlab.com/takuoh/config-artix/-/blob/57b5d808f0d4f9c6b9a7c7b443bbadacc3528174/dotfiles/.config/X11/profile#L10

## Qt

QT_SCREEN_SCALE_FACTORSはスクリーンごとにスケール設定できる。UIのみをスケール

https://gitlab.com/takuoh/config-artix/-/blob/301eb4d18668edec81c0651a9c9e08234c1cb776/dotfiles/.config/X11/profile#L8-9

## alacritty(winit)

物理スクリーンDPIを使用

https://gitlab.com/takuoh/config-artix/-/blob/3530e253e1aed90b8bcf0eb286ed35bfed15b269/dotfiles/.config/X11/profile#L12

## rofi

物理スクリーンDPIを使用

https://gitlab.com/takuoh/config-artix/-/blob/3530e253e1aed90b8bcf0eb286ed35bfed15b269/dotfiles/.config/rofi/config.rasi#L14

# WAYLAND

## Sway

GTKの場合、小数スケールすると1.5の場合、2倍スケールした後0.75倍で縮小スケールしているので無駄にリソースを使う。

waydroid, flameshotでおかしくなる。[flameshot issue#1705](https://github.com/flameshot-org/flameshot/issues/1705)

https://gitlab.com/takuoh/config-artix/-/blob/daa808ba54b7890662f989731fa1a806b6c3008c/dotfiles/.config/sway/config#L323

## foot

自動でやってくれる

https://gitlab.com/takuoh/config-artix/-/blob/3530e253e1aed90b8bcf0eb286ed35bfed15b269/dotfiles/.config/foot/foot.ini#L23

## Mouse Pointer

swayの場合マウスカーソルもスケールするので小さいサイズのカーソルを設定

- [sway 24](https://gitlab.com/takuoh/config-artix/-/blob/daa808ba54b7890662f989731fa1a806b6c3008c/dotfiles/.config/sway/config#L327)

- [X11 32](https://gitlab.com/takuoh/config-artix/-/blob/57b5d808f0d4f9c6b9a7c7b443bbadacc3528174/dotfiles/.config/X11/xresources#L1)

- [cinnamon 32](https://gitlab.com/takuoh/config-artix/-/blob/2646ae5b877c59503e175d9e527762043f4b728d/docs/cinnamon.txt#L21)

- [gtk](https://gitlab.com/takuoh/config-artix/-/blob/3626b3a117398b717633b67fd35d2a43fc136221/dotfiles/.config/gtk-3.0/settings.ini#L6)

# Misc

## Console

terminus-fontをインストール、設定

https://gitlab.com/takuoh/config-artix/-/blob/0f3827d57eca2871ecd498ec66aa07c57a6c6575/etc/vconsole.conf#L1

# memo

## GTK3

GTK3のスケール環境変数"GDK_DPI_SCALE"はマルチモニターにやさしくない。いちいち設定しなおす必要がある

けどgsettingsでtext-scaling-factorを設定するよりはマシ(環境変数を変更するほうが楽)

## java

マルチモニターは_JAVA_OPTIONS書き変え

https://gitlab.com/takuoh/config-artix/-/blob/88ea5e5557bdc0e531789fed67a07f207a0456d1/dotfiles/.config/zsh/.zshrc#L70
