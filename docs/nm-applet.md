# Disable network connection notifications

```bash
gsettings set org.gnome.nm-applet disable-connected-notifications true
```

# Disable network disconnect notification

```bash
gsettings set org.gnome.nm-applet disable-disconnected-notifications true
```
