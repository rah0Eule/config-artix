// VA-API
// https://wiki.archlinux.org/title/firefox#Hardware_video_acceleration
// Disable AV1 decoding
user_pref("media.av1.enabled", false);

// Download
// Preview pdf
user_pref("browser.download.start_downloads_in_tmp_dir", true);
// Confirm when downloading files
user_pref("browser.download.always_ask_before_handling_new_types", true);

// Security
// Do not save passwords
user_pref("signon.rememberSignons", false);
// Enable HTTPS-Only Mode in all windows
user_pref("dom.security.https_only_mode", true);

// https://wiki.archlinux.org/title/Firefox#Right_mouse_button_instantly_clicks_the_first_option_in_window_managers
user_pref("ui.context_menus.after_mouseup", true);

// Turn off disk cache
user_pref("browser.cache.disk.enable", false);

// UserAgent
// Faking useragent and platform
user_pref("general.useragent.override", "Mozilla/5.0 (Windows NT 10.0; rv:102.0) Gecko/20100101 Firefox/102.0");
user_pref("general.platform.override", "Win32");

/* 2810: enable Firefox to clear items on shutdown
 * [SETTING] Privacy & Security>History>Custom Settings>Clear history when Firefox closes | Settings ***/
user_pref("privacy.sanitize.sanitizeOnShutdown", true);

/** SANITIZE ON SHUTDOWN: IGNORES "ALLOW" SITE EXCEPTIONS ***/
/* 2811: set/enforce what items to clear on shutdown (if 2810 is true) [SETUP-CHROME]
 * [NOTE] If "history" is true, downloads will also be cleared
 * [NOTE] "sessions": Active Logins: refers to HTTP Basic Authentication [1], not logins via cookies
 * [1] https://en.wikipedia.org/wiki/Basic_access_authentication ***/
user_pref("privacy.clearOnShutdown.cache", true);     // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.downloads", true); // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.formdata", true);  // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.history", true);   // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.sessions", true);  // [DEFAULT: true]
   // user_pref("privacy.clearOnShutdown.siteSettings", false); // [DEFAULT: false]
/* 2812: set Session Restore to clear on shutdown (if 2810 is true) [FF34+]
 * [NOTE] Not needed if Session Restore is not used (0102) or it is already cleared with history (2811)
 * [NOTE] If true, this prevents resuming from crashes (also see 5008) ***/
   // user_pref("privacy.clearOnShutdown.openWindows", true);

/** SANITIZE ON SHUTDOWN: RESPECTS "ALLOW" SITE EXCEPTIONS FF103+ ***/
/* 2815: set "Cookies" and "Site Data" to clear on shutdown (if 2810 is true) [SETUP-CHROME]
 * [NOTE] Exceptions: A "cookie" block permission also controls "offlineApps" (see note below).
 * serviceWorkers require an "Allow" permission. For cross-domain logins, add exceptions for
 * both sites e.g. https://www.youtube.com (site) + https://accounts.google.com (single sign on)
 * [NOTE] "offlineApps": Offline Website Data: localStorage, service worker cache, QuotaManager (IndexedDB, asm-cache)
 * [WARNING] Be selective with what sites you "Allow", as they also disable partitioning (1767271)
 * [SETTING] to add site exceptions: Ctrl+I>Permissions>Cookies>Allow (when on the website in question)
 * [SETTING] to manage site exceptions: Options>Privacy & Security>Permissions>Settings ***/
user_pref("privacy.clearOnShutdown.cookies", true); // Cookies
user_pref("privacy.clearOnShutdown.offlineApps", true); // Site Data
/* 2816: set cache to clear on exit [FF96+]
 * [NOTE] We already disable disk cache (1001) and clear on exit (2811) which is more robust
 * [1] https://bugzilla.mozilla.org/1671182 ***/
   // user_pref("privacy.clearsitedata.cache.enabled", true);

/** SANITIZE MANUAL: IGNORES "ALLOW" SITE EXCEPTIONS ***/
/* 2820: reset default items to clear with Ctrl-Shift-Del [SETUP-CHROME]
 * This dialog can also be accessed from the menu History>Clear Recent History
 * Firefox remembers your last choices. This will reset them when you start Firefox
 * [NOTE] Regardless of what you set "downloads" to, as soon as the dialog
 * for "Clear Recent History" is opened, it is synced to the same as "history" ***/
user_pref("privacy.cpd.cache", true);    // [DEFAULT: true]
user_pref("privacy.cpd.formdata", true); // [DEFAULT: true]
user_pref("privacy.cpd.history", true);  // [DEFAULT: true]
user_pref("privacy.cpd.sessions", true); // [DEFAULT: true]
user_pref("privacy.cpd.offlineApps", false); // [DEFAULT: false]
user_pref("privacy.cpd.cookies", false);
   // user_pref("privacy.cpd.downloads", true); // not used, see note above
   // user_pref("privacy.cpd.openWindows", false); // Session Restore
   // user_pref("privacy.cpd.passwords", false);
   // user_pref("privacy.cpd.siteSettings", false);
/* 2822: reset default "Time range to clear" for "Clear Recent History" (2820)
 * Firefox remembers your last choice. This will reset the value when you start Firefox
 * 0=everything, 1=last hour, 2=last two hours, 3=last four hours, 4=today
 * [NOTE] Values 5 (last 5 minutes) and 6 (last 24 hours) are not listed in the dropdown,
 * which will display a blank value, and are not guaranteed to work ***/
user_pref("privacy.sanitize.timeSpan", 0);

// Do not show warning when opening about:config
user_pref("browser.aboutConfig.showWarning", false);

// Disable firefox intro tabs on the first start
user_pref("browser.startup.homepage_override.mstone", "ignore");

// Disable Firefox View
user_pref("browser.tabs.firefox-view", false);

// Disable Sponsored shortcuts
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);

// Enable automatic extension installation from /usr/lib/firefox/browser/extensions/* and profile directory only
// https://searchfox.org/mozilla-central/rev/08f7e9ef03dd2a83118fba6768d1143d809f5ebe/toolkit/mozapps/extensions/AddonManager.jsm#4111-4123
// https://mike.kaply.com/2012/02/21/understanding-add-on-scopes/
// 1 + 4 = 5
user_pref("extensions.enabledScopes", 5);

// Do not open "Firefox Privacy Policy" tab in first launch
user_pref("datareporting.policy.firstRunURL", "");

// Set Proportional font to sans-serif in latin
user_pref("font.default.x-western", "sans-serif");

// Enable userChrome.css
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

// Tracking protection
user_pref("privacy.trackingprotection.enabled", true);

// WebRTC LAN IP Address
user_pref("media.peerconnection.ice.default_address_only", true);

// Disable connection tests
user_pref("network.captive-portal-service.enabled", false);

// Disable telemetry
user_pref("toolkit.telemetry.enabled", false);
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);

// Enable titlebar
user_pref("browser.tabs.inTitlebar", 0);

// Check syntax
user_pref("_user.js.parrot", "Everything is OK.");
