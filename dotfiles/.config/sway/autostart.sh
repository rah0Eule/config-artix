#!/usr/bin/env sh
nm-applet &
runsvdir ~/.config/runit/runsvdir &
/usr/lib/polkit-kde-authentication-agent-1 &
mako &
gammastep &
pcmanfm-qt -d &
fcitx5 &
/usr/lib/kdeconnectd &
keepassxc &
/usr/bin/gnome-keyring-daemon --start --components=pkcs11,ssh
wl-paste -t text --watch clipman store --no-persist &
WOBSOCK=$XDG_RUNTIME_DIR/wob.sock
rm -f $WOBSOCK && mkfifo $WOBSOCK && tail -f $WOBSOCK | xob &
