#!/usr/bin/env sh
dbus-update-activation-environment DISPLAY XAUTHORITY
picom -b
#imwheel
gammastep -m randr &
pcmanfm-qt -d &
fcitx5 &
/usr/lib/kdeconnectd &
keepassxc &
/usr/lib/polkit-kde-authentication-agent-1 &
dunst &
/usr/bin/gnome-keyring-daemon --start --components=pkcs11,ssh
xfce4-clipman &
xss-lock --transfer-sleep-lock ~/.config/i3/transfer-sleep-lock-i3lock.sh &
pasystray &
XOBSOCK=$XDG_RUNTIME_DIR/xob.sock
rm -f $XOBSOCK && mkfifo $XOBSOCK && tail -f $XOBSOCK | xob &
nm-applet &
caffeine &
